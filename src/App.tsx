import React, { createRef } from "react";
import * as linearAlgebra from "./matrix";
import * as collide from "./program/circle-collision";
import Program from "./program";
import "./App.css";

class App extends React.Component {
  private canvasRef: React.RefObject<HTMLCanvasElement>;
  constructor(p:{}){
    super(p);
    this.canvasRef = createRef<HTMLCanvasElement>();
  }

  public componentDidMount() {
    const canvasElement = this.canvasRef.current;
    if (canvasElement) {
      const program = new Program(canvasElement);
      program.start();
    }

    collide.collide(new collide.Ball(new linearAlgebra.Vec2(0,10), 1, new linearAlgebra.Vec2(0,-1)), new collide.Ball(new linearAlgebra.Vec2(10, 0), 1, new linearAlgebra.Vec2(-1, 0)));
    Object.assign(window, linearAlgebra);
  }

  public render() {
    return (
      <div className="App">
        <canvas ref={this.canvasRef}></canvas>
      </div>
    );
  }
}

export default App;
