import Vec2 from "./Vec2";

export class Mat2 {
    private data: number[];

    constructor(...data: number[]) {
        this.data = [1, 0, 0, 1];

        for (let i = 0; i < Math.min(data.length, 4); i++) {
            this.data[i] = data[i];
        }
    }

    public det() {
        const d = this.data;
        return d[0] * d[3] - d[1] * d[2];
    }

    public inv() {
        const d = this.data;
        const det = this.det();
        if (det === 0) return null;
        return Mat2.mul(new Mat2(d[3], -d[1], -d[2], d[0]), 1 / det);
    }

    public clone() {
        return new Mat2(...this.data);
    }

    public row(index: number): Vec2 {
        return new Vec2(this.data[index * 2], this.data[index * 2 + 1]);
    }

    public col(index: number): Vec2 {
        return new Vec2(this.data[index], this.data[index + 2]);
    }

    public transform(v: Vec2): Vec2 {
        const d = this.data;
        return new Vec2(d[0] * v.x + d[1] * v.y, d[2] * v.x + d[3] * v.y);
    }

    public static mul(a: Mat2, b: number): Mat2;
    public static mul(a: Mat2, b: Mat2): Mat2;
    public static mul(a: Mat2, b: Mat2 | number) {
        if (b instanceof Mat2) {
            const ar = [];
            for (let y = 0; y < 2; y++) {
                for (let x = 0; x < 2; x++) {
                    ar[y * 2 + x] = Vec2.dot(a.row(y), b.col(x));
                }
            }
            return new Mat2(...ar);
        }
        if (typeof b === "number") {
            const ar = [];
            for (let i = 0; i < a.data.length; i++) {
                ar[i] = a.data[i] * b;
            }
            return new Mat2(...ar);
        }
        throw new Error("wrong input type");
    }
}

export default Mat2;
