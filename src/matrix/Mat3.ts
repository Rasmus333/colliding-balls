import Vec2 from "./Vec2";
import Vec3 from "./Vec3";
import Mat2 from "./Mat2";

export class Mat3 {
    private data: number[];

    constructor(...data: number[]) {
        this.data = [1, 0, 0, 0, 1, 0, 0, 0, 1];

        for (let i = 0; i < Math.min(data.length, 9); i++) {
            this.data[i] = data[i];
        }
    }

    private getMat2At(index: number): Mat2 {
        const x = index % 3;
        const y = Math.floor(index / 3);

        return new Mat2(
            this.data[index],
            this.data[3 * y + ((x + 1) % 3)],
            this.data[3 * ((y + 1) % 3) + x],
            this.data[3 * ((y + 1) % 3) + ((x + 1) % 3)]
        );
    }

    public det() {
        const d = this.data;
        return (
            d[0] * (d[4] * d[8] - d[5] * d[7]) +
            d[1] * (d[5] * d[6] - d[3] * d[8]) +
            d[2] * (d[3] * d[7] - d[4] * d[6])
        );
    }

    public adj() {
        const d = this.data;
        return new Mat3(
            d[4] * d[8] - d[5] * d[7],
            d[7] * d[2] - d[8] * d[1],
            d[1] * d[5] - d[2] * d[4],
            d[5] * d[6] - d[3] * d[8],
            d[8] * d[0] - d[6] * d[2],
            d[2] * d[3] - d[0] * d[5],
            d[3] * d[7] - d[4] * d[6],
            d[6] * d[1] - d[7] * d[0],
            d[0] * d[4] - d[1] * d[3]
        );
    }

    public inv() {
        const det = this.det();
        if (det === 0) return null;
        return Mat3.mul(this.adj(), 1 / det);
    }

    public clone() {
        return new Mat3(...this.data);
    }

    public row(index: number): Vec3 {
        return new Vec3(
            this.data[index * 3],
            this.data[index * 3 + 1],
            this.data[index * 3 + 2]
        );
    }

    public col(index: number): Vec3 {
        return new Vec3(
            this.data[index],
            this.data[index + 3],
            this.data[index + 6]
        );
    }

    public transform(v: Vec2): Vec2;
    public transform(v: Vec3): Vec3;
    public transform(v: Vec2 | Vec3): Vec2 | Vec3 {
        const d = this.data;
        if (v instanceof Vec2) {
            return new Vec2(
                d[0] * v.x + d[1] * v.y + d[2],
                d[3] * v.x + d[4] * v.y + d[5]
            );
        }
        if (v instanceof Vec3) {
            return new Vec3(
                d[0] * v.x + d[1] * v.y + d[2] * v.z,
                d[3] * v.x + d[4] * v.y + d[5] * v.z,
                d[6] * v.x + d[7] * v.y + d[8] * v.z
            );
        }
        throw new Error("Input was not a vector.");
    }

    public static mul(a: Mat3, b: number): Mat3;
    public static mul(a: Mat3, b: Mat3): Mat3;
    public static mul(a: Mat3, b: Mat3 | number): Mat3 {
        if (b instanceof Mat3) {
            const ar = [];
            for (let y = 0; y < 3; y++) {
                for (let x = 0; x < 3; x++) {
                    ar[y * 3 + x] = Vec3.dot(a.row(y), b.col(x));
                }
            }
            return new Mat3(...ar);
        }
        if (typeof b === "number") {
            const ar = [];
            for (let i = 0; i < a.data.length; i++) {
                ar[i] = a.data[i] * b;
            }
            return new Mat3(...ar);
        }
        throw new Error("wrong input type");
    }

    public static translate(translate: Vec2) {
        return new Mat3(1, 0, translate.x, 0, 1, translate.y);
    }
}

export default Mat3;
