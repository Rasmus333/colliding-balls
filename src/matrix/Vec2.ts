class Vec2 {
    public x: number;
    public y: number;

    constructor(x: number, y: number) {
        this.x = x;
        this.y = y;
    }

    public clone() {
        return new Vec2(this.x, this.y);
    }

    public get length() {
        return Math.sqrt(Vec2.dot(this, this));
    }

    public get normal(): Vec2 {
        return Vec2.mul(this, 1 / this.length);
    }

    public get neg() {
        return new Vec2(-this.x, -this.y);
    }

    public static add(a: Vec2, b: Vec2) {
        return new Vec2(a.x + b.x, a.y + b.y);
    }

    public static sub(a: Vec2, b: Vec2) {
        return new Vec2(a.x - b.x, a.y - b.y);
    }

    public static mul(a: Vec2, b: number) {
        return new Vec2(a.x * b, a.y * b);
    }

    public static dot(a: Vec2, b: Vec2) {
        return a.x * b.x + a.y * b.y;
    }

    public static get zero() {
        return new Vec2(0, 0);
    }

    public static get one() {
        return new Vec2(1, 1);
    }
}

export default Vec2;
