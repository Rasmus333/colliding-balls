class Vec3 {
    public x: number;
    public y: number;
    public z: number;

    constructor(x: number, y: number, z: number) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public clone() {
        return new Vec3(this.x, this.y, this.z);
    }

    public get length() {
        return Math.sqrt(Vec3.dot(this, this));
    }

    public get normal(): Vec3 {
        return Vec3.mul(this, 1 / this.length);
    }
    
    public get neg() {
        return new Vec3(-this.x, -this.y, -this.z);
    }

    public static add(a: Vec3, b: Vec3) {
        return new Vec3(a.x + b.x, a.y + b.y, a.z + b.z);
    }

    public static sub(a: Vec3, b: Vec3) {
        return new Vec3(a.x - b.x, a.y - b.y, a.z - b.z);
    }

    public static mul(a: Vec3, b: number) {
        return new Vec3(a.x * b, a.y * b, a.z * b);
    }

    public static dot(a: Vec3, b: Vec3) {
        return a.x * b.x + a.y * b.y + a.z * b.z;
    }

    public static get zero() {
        return new Vec3(0, 0, 0);
    }

    public static get one() {
        return new Vec3(1, 1, 1);
    }
}

export default Vec3;
