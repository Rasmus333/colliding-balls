import { Mat3, Vec2 } from "../matrix";

export class Collision {}

export class Ball {
    public position: Vec2;
    public radius: number;
    public velocity: Vec2;

    constructor(position: Vec2, radius: number, velocity: Vec2) {
        this.position = position;
        this.radius = radius;
        this.velocity = velocity;
    }

    clone() {
        return new Ball(
            this.position.clone(),
            this.radius,
            this.velocity.clone()
        );
    }
}

/**
 * Calculates if and when two balls moving in a straight line will collide.
 * @param a
 * @param b
 */
export function collide(a: Ball, b: Ball) {
    // Clone the balls so that no changes are made in the original balls.
    const p = a.clone();
    const c = b.clone();

    /* 
	The individual balls velocity does not matter as long as it 
	stays relative to all other balls. This means that you can 
	change the velocity of one ball without making any difference 
	to the results as long as you change the velocity of all the 
	other balls by an equal amount.
	*/

    /*
    We calculate a's velocity relative to b and set b's velocity 
    to 0.
    */

    p.velocity = Vec2.sub(p.velocity, c.velocity);
    c.velocity = Vec2.zero; // b - b = 0

    /* 
	Adding the radius of 'b' to 'a' and setting the radius of 'a' to
    0 we will have to calculate the collision of a static circle and 
    a moving point wich is a much easier problem to tackle than two 
    moving circles. 
	*/

    c.radius += p.radius;
    p.radius = 0;

    /*
    We can make this even easier by applying a transformation matrix 
    wich puts 'a' in (0, 0) moving in positive y.
    */

    const n = p.velocity.normal;
    const rotation = new Mat3(n.y, -n.x, 0, n.x, n.y, 0, 0, 0, 1);
    const translation = Mat3.translate(p.position.neg);

    c.position = rotation.transform(translation.transform(c.position));
    p.position = Vec2.zero;
    p.velocity = rotation.transform(p.velocity);
    if (c.position.x + c.radius < 0 || 0 < c.position.x - c.radius) return null;
    else {
        const distance =
            c.position.y -
            Math.sqrt(c.radius * c.radius - c.position.x * c.position.x);
    }
    console.log(c, p);
}
