import RenderingEngine from "./renderingEngine";

class Program extends RenderingEngine {
    public render() {
        const offset = (Math.PI * 2) / 3;
        const speed = 0.001;
        const r = 128 + 127 * Math.sin(speed * this.time);
        const g = 128 + 127 * Math.sin(speed * this.time + offset);
        const b = 128 + 127 * Math.sin(speed * this.time - offset);

        this.ctx.fillStyle = `rgb(${[r, g, b].join()})`;
        this.ctx.fillRect(0, 0, this.ctx.canvas.width, this.ctx.canvas.height);
    }
}

export default Program;
