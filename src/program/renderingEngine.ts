abstract class RenderingEngine {
    private _oldTime: number;
    private _currentTime: number;
    private _cxt: CanvasRenderingContext2D;
    private _renderInterval: number = NaN;

    public get timeOfLastUpdate() {
        return this._oldTime;
    }

    public get timeSinceLastUpdate() {
        return this._currentTime - this._oldTime;
    }

    public get time() {
        return this._currentTime;
    }

    public get ctx() {
        return this._cxt;
    }

    constructor(canvas: HTMLCanvasElement) {
        const context = canvas.getContext("2d");
        if (context === null) throw new Error("Could not retrieve 2D context.");
        this._cxt = context;
        this._oldTime = this.getMilliseconds();
        this._currentTime = this.getMilliseconds();
    }

    private getMilliseconds() {
        return new Date().getTime();
    }

    public start() {
        this._oldTime = this._currentTime;
        this._currentTime = this.getMilliseconds();
        const callRender = () => {
            this._oldTime = this._currentTime;
            this._currentTime = this.getMilliseconds();
            this.render();
        };
        this._renderInterval = window.setInterval(callRender, 1000 / 60);
    }

    public abstract render(): void;
}

export default RenderingEngine;
